module Main (main) where

import RIO

import Engine.App (engineMainWith)
import Stage.Loader.Setup qualified as Loader

import Global.Resource.Assets qualified as Assets
import Global.Resource.Font qualified as Font
import Global.Resource.Texture.Base qualified as Base
import Stage.Main.Setup qualified as Main

main :: IO ()
main =
  engineMainWith handoff action
  where
    (handoff, action) =
      Loader.bootstrap
        "LD 48 / Time to think"
        (Font.small Font.configs, Font.large Font.configs)
        (splash, spinner)
        (Assets.load "stuff")
        (\assets -> Main.stackStage assets)

    Base.Collection{black} = Base.sources
    splash = black
    spinner = black
