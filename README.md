## Retrospective

### The Jam

I liked the tempo and amount of new stuff I now have to polish and upstream. Alas, lots of it turned up unrelated to the game.

Unlike the previous jams, the idea struck me the instant I saw the theme.
There was no anxious thinking and searching.
Bam! And the next moment I know what to do.
I even tried to break from it, but no.
Execution, on the other hand, proved quite confusing.

### [The Game](https://ldjam.com/events/ludum-dare/48/time-to-think)

Well, for a game about fuckup prevention... I haven't prevented much of it 😅

I had a roadmap that was good in general, but the timing ended up too strict.

A few precious hours gone towards fixing unexpected crashes and into mucky detours.

### [The Engine](https://gitlab.com/keid)

My STM-worker/IO-observer system proved flexible enough for my needs, but a less-boilerplatey abstraction is needed.

I'm beginning to understand what are the hooks to make rendering composable.

### What went...

* Well: I managed to get the starter project in advance.
  * Meh: The starter is for 3D settings and I went full colored rectangles.
    * Well: The engine didn't care much. It was only a question of trimming down the code.
* Wrong: I skipped the paper phase of game design.
  * Meh: This could be a compo submission if not for all the time wasted in dead-ends.
* Well: I took long walks outside and got enough sleep.
* Well: I did learn and practice a few cool techniques.
* Well: I gained confidence and insight in my engine.
* Well: I made a game about a thing I care about.
* Well: It was good to jam together with fellow haskellers.

> This was a pretty good jam. I'm glad I've participated.

## Activity log

### Day 1

I started by checking out what DearImGui has on this and found a nice [node editor](https://github.com/thedmd/imgui-node-editor).
It looked promising and I tried to follow its simple example.
I made some [additions](https://github.com/haskell-game/dear-imgui.hs/compare/main...dpwiz:ld48) to DearImGui Haskell bindings.
But with 3 hours in I reconsidered adding more C++ bindings and learning how to use them properly.

5 frenetic hours later in search for *something* that can be played I've settled on a silly attack/defense/hp model of interaction between the nodes.

```
"take a walk"
|
+- OK
|  |
|  `- 3 % 1
|
+- "rain" defeated! (0 % 1)
|  |
|  `- "umbrella" attacking: 2 (1 % 1)
|
+- "tired" defeated! (0 % 1)
|  |
|  +- "short nap" attacking: 2 (1 % 1)
|  |  |
|  |  `- "call" defeated! ((-1) % 1)
|  |     |
|  |     `- "do not disturb" attacking: 2 (1 % 1)
|  |
|  `- "light tea" attacking: 2 (3 % 1)
|
`- "call" defeated! ((-1) % 1)
   |
   `- "do not disturb" attacking: 2 (1 % 1)
```

A long march towards model visualization started.

Working with Functor/Foldable/Traversable data is pure joy.
I quickly banged together some workers to unzip my model tree into a bunch of boxes to render with different styles.

**The first major blunder**: an actual tree layout is not that easy to make as I've expected.
After struggling for a while I went to bed.

![](https://i.imgur.com/y4nDzKa.png)

### Day 2

The next morning, I've decided to google up a solution.
And, to my luck, there was a nice one in [diagrams-contrib](https://hackage.haskell.org/package/diagrams-contrib-1.4.4/docs/Diagrams-TwoD-Layout-Tree.html) package.
With some grokking, porting, and cleaning done I've got myself my first properly-positioned tree diagram 🌵

![](https://i.imgur.com/YX6dVNZ.png)

With all the nodes *in place*, it became possible to add labels, wires, etc.

The next 6 hours went into writing low-level boilerplate and musing with styles.

Everything was mostly straightforward and I liked the event and data subsystems I put in my engine.
There was little impediment from the framework, but a little abstraction layer would certainly have helped.

![](https://i.imgur.com/vporhJ5.png)

I've got confident with the interface and turned to actual gameplay.

I went in and added a catalog file for game data that can be unfolded by thinking about stuff.

**The second major blunder**: despite having the "simulation" calculations implemented, the "discovery" game phase wasn't clear.
This was obscured by having a hardcoded tree.

In hindsight, I misplaced something like 3 hours of effort into a feature that required another chunk of UI work and flopped.
It wasn't all the waste of course.

I implemented a "focus" mechanic, timers and managed to wrangle some lenses to track a current node.

https://imgur.com/a/VMrZedV


### Day 3

> That's no compo, but there's a good chance I will make it into jam.

A contingency plan was set to get "discovery" into something fun even with less content at the start.
If the catalog file works, the users can pile up more of the User Generated Content during the rating weeks.

In a few hours, I've made something that works with the loaded file instead of hardcode.
And it was good, but rather short.

According to the plan I now have to add polish to make it into an actual game.

In the following 10 hours, mixed with a day job and family time I only added camera animation and message log.

![](https://i.imgur.com/zBMWhGr.png)

For the final hours, I went brave (perhaps too brave) and tried to add the "simulation" stage.

At first, everything went fine and with an unhealthy amount of copy-pasting, I made a data hand-off between the discovery and simulation stages.
Here comes...

**The third major blunder**: I've started to get crashes and sick errors from Vulkan validation layers.
Welp, here goes my "fun game mechanics" time 😰

As a final contingency measure, I added a "load up all those yamls" feature.

Clean up, build package, register game pages, add minimal descriptions, publish, sleep.
