module Main where

import RIO
import Prelude (putStrLn)

import Data.Tree
import Test.Tasty
import Test.Tasty.HUnit
import Text.Pretty.Simple (pPrint)
import Data.Tree.Layout (symmLayout)

import Stage.Main.World.Stuff qualified as Stuff
import Stage.Main.World.Situation qualified as Situation

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
  [ welp3
  ]

welp3 = testCase "unfold gadt" do
  let
    x = Situation.unfoldTask Stuff.testTask
    t = Situation.toTree x
    -- fixed = Situation.fixRoot x
  liftIO $ putStrLn . drawTree . fmap (show . snd) $ symmLayout 0 t

  -- liftIO . pPrint $ Situation.poop fixed

-- welp2 :: TestTree
-- welp2 = testCase "nodes" do
--   let
--     (taskRemain, resolutions) = runTask park
--   putStrLn . drawTree $
--     Node (show $ tLabel park)
--       $ Node (if taskRemain > 0 then "OK" else "defeated!") [Node (show taskRemain) []]
--       : map (fmap showResolution) resolutions

-- data Resolution = Resolution
--   { rLabel  :: Text
--   , rAttack :: Maybe Integer
--   , rRemain :: Rational
--   }
--   deriving (Show)

-- showResolution Resolution{..} = unwords
--   [ show rLabel
--   , maybe "defeated!" (mappend "attacking: " . show) rAttack
--   , "(" <> show rRemain <> ")"
--   ]

-- runTask :: Task -> (Rational, Forest Resolution)
-- runTask Task{..} = (remain, problems)
--   where
--     resolution = Resolution
--       { rLabel  = tLabel
--       , rAttack = Nothing
--       , rRemain = remain
--       }

--     remain = tHitPoints - sum incoming % tDefence
--     incoming = catMaybes $ map (rAttack . rootLabel) problems
--     problems = map runProblem tProblems

-- runProblem Problem{..} = Node resolution solutions
--   where
--     resolution = Resolution
--       { rLabel  = pLabel
--       , rAttack = attack
--       , rRemain = remain
--       }
--     attack =
--       if remain > 0 then
--         Just pAttack
--       else
--         Nothing

--     remain = pHitPoints - sum incoming % pDefence
--     incoming = catMaybes $ map (rAttack . rootLabel) solutions

--     solutions = map runSolution pSolutions

-- runSolution Solution{..} = Node resolution problems
--   where
--     resolution = Resolution
--       { rLabel  = sLabel
--       , rAttack = attack
--       , rRemain = remain
--       }

--     attack =
--       if remain > 0 then
--         Just sAttack
--       else
--         Nothing

--     remain = sHitPoints - sum incoming % sDefence
--     incoming = catMaybes $ map (rAttack . rootLabel) problems

--     problems = map runProblem sProblems

-- -- attackProblem :: Problem -> Integer
-- -- attackProblem Problem{..} =
-- --   if remain > 0 then
-- --     traceShow (pLabel, "attacking") pAttack
-- --   else
-- --     traceShow (pLabel, "neutralized", remain) 0
-- --   where
-- --     incoming = sum (map attackSolution pSolutions)
-- --     remain = pHitPoints - incoming % pDefence

-- -- attackSolution :: Solution -> Integer
-- -- attackSolution Solution{..} =
-- --   if remain > 0 then
-- --     traceShow (sLabel, "attacking") sAttack
-- --   else
-- --     traceShow (sLabel, "neutralized", remain) 0
-- --   where
-- --     incoming = sum (map attackProblem sProblems)
-- --     remain = sHitPoints - incoming % sDefence

-- -- _welp :: TestTree
-- -- _welp = testCase "welp" do
-- --   liftIO . pPrint $ updateSim baz

-- -- data SimState = SimState
-- --   { ssChance :: Rational
-- --   }
-- --   deriving (Show)

-- -- updateSim Situation{..} =
-- --   Situation{slack = updated, ..}
-- --   where
-- --     updated = applyWrongs 1 possible

-- -- applyWrongs baseChance possible =
-- --   foldl' (/) baseChance do
-- --     Wrong{label, chance, mitigations} <- possible
-- --     -- traceShowM (label, chance, impact, impact / chance)
-- --     pure $ applyMitigations chance mitigations

-- -- applyMitigations baseChance mitigations =
-- --   foldl' (/) baseChance do
-- --     Mitigation{label, chance, possible} <- mitigations
-- --     -- traceShowM (label, chance, impact, impact / chance)
-- --     pure $ applyWrongs chance possible
