module Data.Tree.Layout where

import RIO

import Data.List (mapAccumL, maximum, unzip)
import Data.Tree (Tree(..))
import Geomancy (Vec2, vec2, withVec2, pattern WithVec2)

newtype Extent = Extent { getExtent :: [Vec2] }

instance Semigroup Extent where
  (<>) = mergeExtents

instance Monoid Extent where
  mempty  = Extent []

-- | Merge two non-overlapping extents.
mergeExtents :: Extent -> Extent -> Extent
mergeExtents (Extent e1) (Extent e2) = Extent $ mergeExtents' e1 e2
  where
    mergeExtents' [] qs = qs
    mergeExtents' ps [] = ps
    mergeExtents' (WithVec2 p _ : ps) (WithVec2 _ q : qs) = vec2 p q : mergeExtents' ps qs

extent :: ([Vec2] -> [Vec2]) -> Extent -> Extent
extent  f = Extent . f . getExtent

consExtent :: Vec2 -> Extent -> Extent
consExtent = extent . (:)

-- | Shift an extent horizontally.
moveExtent :: Float -> Extent -> Extent
moveExtent x = (extent . map) (+ vec2 x x)

flipExtent :: Extent -> Extent
flipExtent = (extent . map) (\v -> withVec2 v \p q -> vec2 (-q) (-p))

--------------

-- | Determine the amount to shift in order to \"fit\" two extents
--   next to one another.  The first argument is the separation to
--   leave between them.
fit :: Float -> Extent -> Extent -> Float
fit hSep (Extent ps) (Extent qs) =
  maximum $
    0 : zipWith (\(WithVec2 _ p) (WithVec2 q _) -> p - q + hSep) ps qs

-- | Fit a list of subtree extents together using a left-biased
--   algorithm.  Compute a list of positions (relative to the leftmost
--   subtree which is considered to have position 0).
fitListL :: Float -> [Extent] -> [Float]
fitListL hSep = snd . mapAccumL fitOne mempty
  where
    fitOne acc e =
      let
        x = fit hSep acc e
      in
        ( acc <> moveExtent x e
        , x
        )

-- | Fit a list of subtree extents together with a right bias.
fitListR :: Float -> [Extent] -> [Float]
fitListR hSep =
  reverse .
  map negate .
  fitListL hSep .
  map flipExtent .
  reverse

-- | Compute a symmetric fitting by averaging the results of left- and
--   right-biased fitting.
fitList :: Float -> [Extent] -> [Float]
fitList hSep = uncurry (zipWith mean) . (fitListL hSep &&& fitListR hSep)
  where
    mean x y = (x + y) / 2

--------------

type Rel t a = t (a, Float)

-- | Given a fixed location for the root, turn a tree with
--   \"relative\" positioning into one with absolute locations
--   associated to all the nodes.
unRelativize :: Show a => Vec2 -> Rel Tree a -> Tree (a, Vec2)
unRelativize curPt (Node (a, hOffs) ts) =
  Node
    (a, rootPt)
    (map
      (unRelativize $
        rootPt + vec2 0 vOffs
      )
      ts
    )
  where
    rootPt = curPt + vec2 hOffs 0

    vOffs =
      slVSep +
      withVec2 (slHeight a) (\top _bottom -> -top) +
      (maximum do
        Node (n, _) _ <- ts
        pure $ withVec2 (slHeight n) \_top bottom ->
          bottom
      )

-- | Shift a RelTree horizontally.
moveTree :: Float -> Rel Tree a -> Rel Tree a
moveTree offset (Node (a, x) ts) = Node (a, x + offset) ts

-- | Run the symmetric rose tree layout algorithm on a given tree,
--   resulting in the same tree annotated with node positions.
symmLayout :: Show a => Vec2 -> Tree a -> Tree (a, Vec2)
symmLayout origin = unRelativize origin . fst . symmLayoutR

-- | Actual recursive tree layout algorithm, which returns a tree
--   layout as well as an extent.
symmLayoutR :: Show a => Tree a -> (Rel Tree a, Extent)
symmLayoutR (Node a ts) = (rt, ext)
  where
    rt               = Node (a, 0) pTrees
    pTrees           = zipWith moveTree positions trees

    ext              = consExtent (slWidth a) (mconcat pExtents)
    pExtents         = zipWith moveExtent positions extents

    (trees, extents) = unzip (map symmLayoutR ts)
    positions        = fitList slHSep extents

slHSep :: Float
slHSep = 32

slVSep :: Float
slVSep = 32

slWidth :: a -> Vec2
slWidth _ = vec2 (-80) 80

slHeight :: a -> Vec2
slHeight _ = vec2 (-45) 45
