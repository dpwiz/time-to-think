module Stage.Sim.World.UI
  ( UI(..)
  , spawn

  , postMessage_
  , postMessageRare
  , postMessage

  , Observer(..)
  , newObserver
  , observe
  , readObserved
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Geomancy (Transform, Vec2, Vec4, vec2, vec4)
import RIO.List (unzip3)
import RIO.Vector.Storable qualified as Storable
-- import Text.Printf (printf)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.Buffer qualified as Buffer

import Global.Resource.Assets (Assets(..))
import Global.Resource.Combined qualified as Combined
import Global.Resource.Font qualified as GlobalFont
import Stage.Main.Resource.Stuff qualified as GameStuff
import Stage.Main.World.Scene qualified as Scene

data UI = UI
  { messageLines   :: [Worker.Var (Float, Vec4, Text)]
  , messageWorkers :: [Message.Process]
  , messageBgP     :: Worker.Merge (Storable.Vector Transform)

  , cameraP :: Worker.Timed () ()
  }

spawn
  :: ( Worker.HasOutput cursor
     , Worker.GetOutput cursor ~ Vec2
     )
  => Assets
  -> Layout.BoxProcess
  -> cursor
  -> Scene.ViewProcess
  -> Engine.StageRIO env (Resource.ReleaseKey, UI)
spawn assets screenP cursorP viewVar = do
  (messageBoxKey, messageBoxP) <- Worker.registered $
    Layout.fitPlaceAbs
      Layout.CenterBottom
      (vec2 1024 $ sum lineSizes)
      screenP

  lineBoxes <- Layout.splitsRelStatic Layout.sharePadsV messageBoxP lineSizes
  lineBoxesKey <- Worker.registerCollection lineBoxes

  messageLines <- for lineSizes \lineSize ->
    Worker.newVar (lineSize, 1, "")

  messages <- for (zip lineBoxes messageLines) \(lineBoxP, messageVar) ->
    Message.spawnFromR lineBoxP messageVar \(size, color, text) ->
      messageLine size color text
  let (inputKeys, messageKeys, messageWorkers) = unzip3 messages

  (messageBgKey, messageBgP) <- Worker.registered $
    Worker.spawnMerge1 (Storable.singleton . Layout.boxTransformAbs) messageBoxP

  (cameraKey, cameraP) <- Worker.registered $
    spawnCamera cursorP viewVar

  key <- Resource.register $ traverse_ Resource.release $ mconcat
    [ [ messageBoxKey
      , lineBoxesKey
      , messageBgKey
      , cameraKey
      ]
    , inputKeys
    , messageKeys
    ]

  pure (key, UI{..})
  where
    messageLine size color text = Message.Input
      { inputText         = text
      , inputOrigin       = Layout.CenterBottom
      , inputSize         = size
      , inputColor        = color
      , inputFont         = GlobalFont.small aFonts
      , inputFontId       = GlobalFont.small $ CombinedTextures.fonts Combined.indices
      , inputOutline      = vec4 0 0 0 1
      , inputOutlineWidth = 2/32
      , inputSmoothing    = 4/32
      }

    Assets{aFonts} = assets

lineSizes :: [Float]
lineSizes = replicate 5 22

numLines :: Int
numLines = length lineSizes

spawnCamera
  :: ( MonadUnliftIO m
     , Worker.HasOutput cursor
     , Worker.GetOutput cursor ~ Vec2
     )
  => cursor
  -> Scene.ViewProcess
  -> m (Worker.Timed () ())
spawnCamera cursorP viewVar = Worker.spawnTimed_ False 10_000 () stepF
  where
    alpha = 1/128

    stepF = atomically do
      pos <- fmap Worker.vData . readTVar $ Worker.getOutput cursorP
      Worker.pushInputSTM viewVar \(pan, zoom) ->
        ( pan * (1 - alpha) + pos * alpha
        , zoom -- TODO: fix cursorPos zoom in MouseButton
        )

-- * Updates

postMessage :: UI -> Float -> Vec4 -> Text -> STM ()
postMessage UI{..} size color text = do
  current <- traverse (fmap Worker.vData . readTVar) messageLines
  let new = take numLines $ (size, color, text) : map fade current
  for_ (zip messageLines new) \(var, input) ->
    Worker.pushInputSTM var (const input)
  where
    fade (s, c, t) = (max 16 $ s - 1, c * 0.9, t)

postMessageRare :: UI -> GameStuff.Rarity -> Text -> STM ()
postMessageRare ui rarity text =
  postMessage ui (fromIntegral $ 16 + fromEnum rarity) (rareColor rarity) text

postMessage_ :: UI -> Text -> STM ()
postMessage_ ui = postMessage ui 16 0.9

rareColor :: GameStuff.Rarity -> Vec4
rareColor = \case
  GameStuff.Common    -> rgbI 202 202 202
  GameStuff.Uncommon  -> rgbI 128 207  63
  GameStuff.Rare      -> rgbI  47 213   1
  GameStuff.Epic      -> rgbI 189  63 250
  GameStuff.Legendary -> rgbI 253 174  83
  GameStuff.Mythic    -> rgbI 252 221 121
  where
    rgbI r g b = vec4 (r/255) (g/255) (b/255) 1

-- * Output

data Observer = Observer
  { messages   :: [Message.Observer]
  , messagesBg :: BgObserver
  }

type BgBuffer = Buffer.Allocated 'Buffer.Coherent Transform
type BgObserver = Worker.ObserverIO BgBuffer

newObserver :: UI -> ResourceT (Engine.StageRIO st) Observer
newObserver UI{..} = do
  messages <- traverse (const $ Message.newObserver 256) messageWorkers
  messagesBg <- newBgObserver

  pure Observer{..}

newBgObserver :: ResourceT (Engine.StageRIO st) BgObserver
newBgObserver = do
  context <- ask

  (_transient, initialData) <-
    Buffer.allocateCoherent
      context
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      1
      mempty

  Worker.newObserverIO initialData

observe
  :: ( HasVulkan env
     )
  => UI
  -> Observer
  -> RIO env ()
observe UI{..} Observer{..} = do
  traverse_ (uncurry Message.observe) $
    zip messageWorkers messages

  context <- ask
  Worker.observeIO_ messageBgP messagesBg $
    Buffer.updateCoherentResize_ context

readObserved
  :: MonadUnliftIO m
  => Observer
  -> m ([Message.Buffer], BgBuffer)
readObserved Observer{..} = (,)
  <$> traverse Worker.readObservedIO messages
  <*> Worker.readObservedIO messagesBg
