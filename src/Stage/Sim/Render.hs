{-# LANGUAGE OverloadedLists #-}

module Stage.Sim.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO

import RIO.State (gets)
import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa

import Global.Resource.Assets (Assets(..))
import Global.Resource.Model qualified as Model
import Stage.Main.World.Situation qualified as Situation
import Stage.Sim.Types (FrameResources(..), RunState(..))
import Stage.Sim.World.UI qualified as UI

updateBuffers
  :: RunState
  -> FrameResources
  -> Basic.StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Set0.observe rsSceneP frScene
  Set0.observe rsSceneUiP frSceneUi

  UI.observe rsUI frUI

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> Basic.StageFrameRIO FrameResources RunState ()
recordCommands cb FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask
  let Basic.Pipelines{..} = fPipelines

  Model.Collection{..} <- gets $ aModels . rsAssets

  Situation.SnapshotData{..} <- gets rsSnapshotData

  (messages, messageBg) <- UI.readObserved frUI

  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Set0.withBoundSet0 frScene pWireframe cb do
      Pipeline.bind cb pWireframe $
        Draw.indexed cb sdWires zeroTransform

      Pipeline.bind cb pUnlitColored do
        Draw.indexed cb task sdTasks
        Draw.indexed cb problem sdProblems
        Draw.indexed cb solution sdSolutions

      Pipeline.bind cb pEvanwSdf $
        Draw.quads cb sdLabels

    Set0.withBoundSet0 frSceneUi pUnlitColored cb do
      Pipeline.bind cb pUnlitColored do
        Draw.indexed cb shroud messageBg
        -- Draw.indexed cb shroud thinkingBg

      Pipeline.bind cb pEvanwSdf $
        traverse_ (Draw.quads cb) messages

    -- ImGui.draw dear cb
