{-# LANGUAGE OverloadedLists #-}

module Stage.Sim.Setup
  ( stackStage
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
import Engine.Events.CursorPos qualified as CursorPos
import Engine.Events.MouseButton qualified as MouseButton
import Engine.Types (StackStage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.Samplers qualified as Samplers
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer (withPools)

import Global.Resource.Assets (Assets(..))
import Global.Resource.Combined qualified as Combined
import Global.Resource.CubeMap qualified as CubeMap
import Stage.Main.World.Scene qualified as Scene
import Stage.Main.World.Situation qualified as Situation
import Stage.Sim.Event.MouseButton qualified as MouseButton
import Stage.Sim.Event.Sink (handleEvent)
import Stage.Sim.Render qualified as Render
import Stage.Sim.Types (FrameResources(..), RunState(..), Stage)
import Stage.Sim.World.UI qualified as UI

stackStage :: Assets -> Situation.Snapshot -> StackStage
stackStage assets snapshot =
  StackStage $ stage assets snapshot

stage :: Assets -> Situation.Snapshot -> Stage
stage assets snapshot = Engine.Stage
  { sTitle = "Sim"

  , sAllocateP  = allocatePipelines
  , sInitialRS  = initialRunState assets snapshot
  , sInitialRR  = initialFrameResources assets
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sAfterLoop = Resource.release
  }
  where
    allocatePipelines swapchain rps = do
      (_, samplers) <- Samplers.allocate swapchain
      let
        sceneBinds =
          Scene.mkBindings
            samplers
            Combined.sources
            CubeMap.sources
            0 -- no shadows
      Basic.allocatePipelines sceneBinds swapchain rps

    beforeLoop = do
      cursorWindow <- gets rsCursorPos
      cursorCentered <- gets rsCursorP

      (key, sink) <- Events.spawn
        handleEvent
        [ CursorPos.callback cursorWindow
        , MouseButton.callback cursorCentered MouseButton.clickHandler
        -- , Key.callback
        -- , Kontrol.spawn
        ]

      modify' \rs -> rs
        { rsEvents = Just sink
        }

      pure key

initialRunState
  :: Assets
  -> Situation.Snapshot
  -> StageRIO env (Resource.ReleaseKey, RunState)
initialRunState rsAssets rsSnapshot = withPools \pools -> do
  rsProjectionP <- Engine.getScreenP
  rsCursorPos <- Worker.newVar 0
  (cursorKey, rsCursorP) <- Worker.registered $
    Worker.spawnMerge2
      (\Camera.ProjectionInput{projectionScreen} (WithVec2 windowX windowY) ->
        let
          Vk.Extent2D{width, height} = projectionScreen
        in
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      (Worker.getInput rsProjectionP)
      rsCursorPos

  (screenKey, screenBoxP) <- Worker.registered Layout.trackScreen

  rsViewP <- Worker.newVar (0, 1)

  (sceneKey, rsSceneP) <- Worker.registered $
    Worker.spawnMerge2 Scene.mkScene rsProjectionP rsViewP

  (sceneUiKey, rsSceneUiP) <- Worker.registered $
    Worker.spawnMerge1 Scene.mkSceneUi rsProjectionP

  (snapshotKey, rsSnapshotData) <- Situation.snapshotData pools rsSnapshot

  (uiKey, rsUI) <- UI.spawn rsAssets screenBoxP rsCursorP rsViewP

  releaseKeys <- Resource.register $
    traverse_ @[] Resource.release
      [ cursorKey
      , screenKey
      , sceneKey
      , sceneUiKey
      , uiKey
      , snapshotKey
      ]

  let rsEvents = Nothing

  pure (releaseKeys, RunState{..})

initialFrameResources
  :: Assets
  -> Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Basic.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources Assets{aTextures} pools _passes pipelines = do
  context <- ask

  frScene <- Scene.allocate
    (Basic.getSceneLayout pipelines)
    aTextures
    Nothing
    Nothing
    mempty
    Nothing

  frSceneUi <- Scene.allocate
    (Basic.getSceneLayout pipelines)
    aTextures
    Nothing
    Nothing
    mempty
    Nothing

  frZeroTransform <- Buffer.createStaged
    context
    pools
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    [mempty]
  void . Resource.register $
    Buffer.destroy context frZeroTransform

  ui <- gets rsUI
  frUI <- UI.newObserver ui

  pure FrameResources{..}
