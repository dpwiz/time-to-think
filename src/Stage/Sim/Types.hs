module Stage.Sim.Types
  ( Stage
  , Frame

  , Assets
  , FrameResources(..)
  , RunState(..)
  ) where

import RIO

import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
import Engine.Worker qualified as Worker
import Geomancy (Transform, Vec2)
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Resource.Buffer qualified as Buffer

import Global.Resource.Assets (Assets)
import Stage.Main.World.Scene qualified as Scene
import Stage.Main.World.Situation qualified as Situation
import Stage.Sim.Event.Type (Event)
import Stage.Sim.World.UI (UI)
import Stage.Sim.World.UI qualified as UI

type Stage = Basic.Stage FrameResources RunState

type Frame = Basic.Frame FrameResources

data FrameResources = FrameResources
  { frScene   :: Set0.FrameResource '[Set0.Scene]
  , frSceneUi :: Set0.FrameResource '[Set0.Scene]

  , frZeroTransform :: Buffer.Allocated 'Buffer.Staged Transform

  , frUI :: UI.Observer
  }

data RunState = RunState
  { rsEvents :: Maybe (Events.Sink Event RunState)
  , rsAssets :: Assets

  , rsProjectionP :: Camera.ProjectionProcess
  , rsViewP       :: Scene.ViewProcess

  , rsCursorPos :: Worker.Var Vec2
  , rsCursorP   :: Worker.Merge Vec2

  , rsSceneP :: Scene.Process
  -- , rsSceneV :: Scene.InputVar

  , rsSceneUiP :: Scene.Process

  , rsSnapshot     :: Situation.Snapshot
  , rsSnapshotData :: Situation.SnapshotData

  , rsUI :: UI
  }
