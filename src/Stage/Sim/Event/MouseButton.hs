module Stage.Sim.Event.MouseButton
  ( clickHandler
  ) where

import RIO

import RIO.State (gets)
-- import RIO.NonEmpty.Partial qualified as NonEmpty (fromList)

import Engine.Events.MouseButton (ClickHandler)
import Engine.Events.Sink (Sink(..))
-- import Engine.UI.Layout qualified as Layout
import Engine.Window.MouseButton (MouseButton(..), MouseButtonState(..))
import Engine.Worker qualified as Worker

import Stage.Sim.Event.Type (Event)
-- import Stage.Sim.Event.Type qualified as Event
import Stage.Sim.Types (RunState(..))
import Stage.Sim.World.UI qualified as UI

clickHandler :: ClickHandler Event RunState
clickHandler (Sink _signal) uiPos buttonEvent = do
  ui <- gets rsUI

  -- (pan, zoom) <- gets rsViewP >>=
  --   Worker.getInputData

  case buttonEvent of
    (_mods, MouseButtonState'Pressed, MouseButton'1) ->
      atomically $ writeTVar (Worker.tActive $ UI.cameraP ui) True

    (_mods, MouseButtonState'Released, MouseButton'1) ->
      atomically $ writeTVar (Worker.tActive $ UI.cameraP ui) False

    _ ->
      logInfo $ "MouseButton event: " <> displayShow (uiPos, buttonEvent)
