module Stage.Sim.Event.Sink
  ( handleEvent
  ) where

import RIO

-- import RIO.State (gets)

import Engine.Types (StageRIO)
-- import Engine.Worker qualified as Worker

import Stage.Sim.Event.Type (Event(..))
import Stage.Sim.Types (RunState(..))
-- import Stage.Sim.World.UI qualified as UI

handleEvent :: Event -> StageRIO RunState ()
handleEvent = \case
  Restart ->
    pure ()
