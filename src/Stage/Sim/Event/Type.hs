module Stage.Sim.Event.Type
  ( Event(..)
  ) where

import RIO

data Event
  = Restart
  deriving (Show, Generic)
