{-# LANGUAGE OverloadedLists #-}

{-# OPTIONS_GHC -Wwarn=type-defaults -Wwarn=deprecations #-}

module Stage.Main.World.Thinking where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Types (StageRIO)
import Engine.UI.Layout qualified as Layout
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Geomancy.Transform qualified as Transform
import Resource.Buffer qualified as Buffer
import RIO.Lens (ix)
import RIO.List (sortOn, (\\))
import RIO.Text qualified as Text
import RIO.Map qualified as Map
import RIO.Set qualified as Set
import RIO.Vector.Storable qualified as Storable

import Stage.Main.Resource.Stuff qualified as GameStuff
import Stage.Main.World.Scene qualified as Scene
import Stage.Main.World.Situation qualified as Situation
import Stage.Main.World.Stuff qualified as Stuff
import Stage.Main.UI (UI)
import Stage.Main.UI qualified as UI

data Process = Process
  { focusPath :: Worker.Var (Maybe [Int])
  , focusNode :: Worker.Merge (Maybe FocusNode)
  , focusBox  :: Worker.Merge (Storable.Vector Transform)

  , knownProblems  :: Worker.Merge (Set Text)
  , knownSolutions :: Worker.Merge (Set Text)

  , thinkTimer :: Worker.Timed () ()
  , focusTimer :: Worker.Timed () ()
  }

spawn
  :: HasLogFunc env
  => UI
  -> Situation.Process
  -> GameStuff.Catalog
  -> Scene.ViewProcess
  -> RIO env Process
spawn ui situation catalog camera = do
  focusPath <- Worker.newVar Nothing
  focusNode <- Worker.spawnMerge2 mkFocusNode layoutP focusPath
  focusBox  <- Worker.spawnMerge1 mkFocusBox focusNode

  knownProblems  <- Worker.spawnMerge1 trackProblems layoutP
  knownSolutions <- Worker.spawnMerge1 trackSolutions layoutP

  thinkTimer <- spawnThinkTimer
    ui
    situation
    catalog
    focusPath
    focusNode
    knownProblems
    knownSolutions

  focusTimer <- spawnFocusTimer camera focusNode

  pure Process{..}
  where
    layoutP = Situation.layout situation

type FocusNode = (Vec2, Situation.SomeStuff)

mkFocusNode
  :: Situation.TreeLayout
  -> Maybe [Int]
  -> Maybe FocusNode
mkFocusNode layout mpath = do
  path <- mpath
  ((_path, someStuff), pos) <- Situation.getLayoutPath layout path
  pure (pos, someStuff)

mkFocusBox
  :: Maybe FocusNode
  -> Storable.Vector Transform
mkFocusBox = \case
  Nothing ->
    []
  Just (pos, _someStuff) ->
    [ mappend (Transform.translate 0 0 0.6) . Layout.boxTransformAbs $
        Layout.Box pos (vec2 160 90 + 8)
    ]

trackProblems :: Situation.LayoutTree -> Set Text
trackProblems = foldMap \((_ann, someStuff), _pos) ->
  case someStuff of
    Situation.Problem Stuff.Problem{pLabel} ->
      Set.singleton pLabel
    _ ->
      mempty

trackSolutions :: Situation.LayoutTree -> Set Text
trackSolutions = foldMap \((_ann, someStuff), _pos) ->
  case someStuff of
    Situation.Solution Stuff.Solution{sLabel} ->
      Set.singleton sLabel
    _ ->
      mempty

spawnThinkTimer
  :: ( HasLogFunc env
     , Worker.HasOutput node
     , Worker.GetOutput node ~ Maybe FocusNode
     , Worker.HasOutput known
     , Worker.GetOutput known ~ Set Text
     )
  => UI
  -> Situation.Process
  -> GameStuff.Catalog
  -> Worker.Var (Maybe [Int])
  -> node
  -> known
  -> known
  -> RIO env (Worker.Timed () ())
spawnThinkTimer ui situation catalog pathVar nodeVar problemsVar solutionsVar =
  Worker.spawnTimed_ False 100_000 () do
    logDebug "thinking..."
    -- gen0 <- newStdGen
    atomically do
      Worker.pushInputSTM (UI.thinkingTime ui) \t -> t + 0.1

      mpath <- fmap Worker.vData (readTVar pathVar)
      mnode <- fmap Worker.vData . readTVar $
        Worker.getOutput nodeVar
      problems <- fmap Worker.vData .readTVar $
        Worker.getOutput problemsVar
      solutions <- fmap Worker.vData .readTVar $
        Worker.getOutput solutionsVar

      oldSituation <- fmap Worker.vData . readTVar $
        Worker.getInput $ Situation.layout situation

      for_ ((,) <$> mpath <*> mnode) \(path, (_pos, oldStuff)) -> do
        case path of
          [] -> do
            Worker.pushInputSTM (Situation.layout situation) $
              researchTask catalog problems

            newSituation <- fmap Worker.vData . readTVar . Worker.getInput $
              Situation.layout situation

            let
              oldProblems = reduceP (Stuff.tProblems oldSituation)
              newProblems = reduceP (Stuff.tProblems newSituation)
            for_ (newProblems \\ oldProblems) \(label, known) ->
              postProblem label known

          inTask : deeper -> do
            let
              update = case oldStuff of
                Situation.Problem _p ->
                  Left $ researchProblem catalog solutions
                Situation.Solution _s ->
                  Right $ researchSolution catalog problems
                Situation.Task _t ->
                  -- XXX: worker desync?
                  Left id

            Worker.pushInputSTM (Situation.layout situation) $
              Stuff.taskProblems . ix inTask %~ Stuff.modifyProblem update deeper

            newSituation <- fmap Worker.vData . readTVar . Worker.getInput $
              Situation.layout situation
            let
              mold = Situation.getSituationPath (Situation.Task oldSituation) path
              mnew = Situation.getSituationPath (Situation.Task newSituation) path
            for_ ((,) <$> mold <*> mnew) \case
              (Situation.Problem oldProblem, Situation.Problem newProblem) -> do
                let
                  old = reduceS (Stuff.pSolutions oldProblem)
                  new = reduceS (Stuff.pSolutions newProblem)
                for_ (new \\ old) \(label, known) ->
                  postSolution label known

              (Situation.Solution oldSolution, Situation.Solution newSolution) -> do
                let
                  old = reduceP (Stuff.sProblems oldSolution)
                  new = reduceP (Stuff.sProblems newSolution)
                for_ (new \\ old) \(label, known) ->
                  postProblem label known

              (_old, _new) ->
                -- traceShowM (Situation.stuffLabel old, Situation.stuffLabel new)
                pure ()
  where
    reduceP = map (Stuff.pLabel &&& Stuff.pKnown)
    reduceS = map (Stuff.sLabel &&& Stuff.sKnown)

    postProblem label known =
      case Map.lookup label (GameStuff.problems catalog) of
        Nothing ->
          -- traceShowM "problem not in catalog"
          pure ()
        Just GameStuff.Problem{discovery} ->
          UI.postMessageRare ui discovery $ Text.unwords
            [ if known then
                "Remembered"
              else
                "Discovered"
            , Text.toLower . Text.pack $ show discovery
            , "problem:"
            , label
            ]

    postSolution label known =
      case Map.lookup label (GameStuff.solutions catalog) of
        Nothing ->
          -- traceShowM "solution not in catalog"
          pure ()
        Just GameStuff.Solution{discovery} ->
          UI.postMessageRare ui discovery $ Text.unwords
            [ if known then
                "Remembered"
              else
                "Discovered"
            , Text.toLower . Text.pack $ show discovery
            , "solution:"
            , label
            ]

spawnFocusTimer
  :: ( Worker.HasInput camera
     , Worker.GetInput camera ~ Scene.View
     , Worker.HasOutput node
     , Worker.GetOutput node ~ Maybe FocusNode
     )
  => camera
  -> node
  -> RIO env (Worker.Timed () ())
spawnFocusTimer camera nodeVar =
  Worker.spawnTimed_ False 10_000 () $
    atomically do
      mnode <- fmap Worker.vData (readTVar $ Worker.getOutput nodeVar)
      for_ mnode \(pos, _someStuff) ->
        Worker.pushInputSTM camera \(pan, zoom) ->
          ( pan * (1 - alpha) + pos * alpha
          , zoom -- TODO: fix cursorPos zoom in MouseButton
          )
  where
    alpha = 1/512

researchTask
  :: GameStuff.Catalog
  -> Set Text
  -> Stuff.Task
  -> Stuff.Task
researchTask catalog known task@Stuff.Task{..} =
  task
    { Stuff.tDiscovery = newDiscovery
    , Stuff.tProblems = newProblems
    }
  where
    newDiscovery = tDiscovery + 0.1

    newProblems =
      if floor newDiscovery > length tProblems then
        -- XXX: very shaky signal when no problems can be created
        tProblems ++ fmap snd (take 1 $ sortOn fst extras)
      else
        tProblems

    extras = do
      GameStuff.Task{tags=taskTags} <- maybeToList $
        Map.lookup tLabel (GameStuff.tasks catalog)

      (key, problem) <- Map.toList (GameStuff.problems catalog)
      guard $ key `notElem` map Stuff.pLabel tProblems

      let
        GameStuff.Problem{for=problemTags, ..} = problem
        match = Set.intersection taskTags problemTags
      guard $ Set.size match > 0
      -- traceShowM (discovery, key, "passed", match)

      pure
        ( discovery
        , Stuff.Problem
          { pLabel     = key
          , pAttack    = attack
          , pDefence   = defence
          , pHitPoints = hp % 1

          , pDiscovery = 0
          , pKnown     = Set.member key known
          , pSolutions = mempty
          }
        )

researchProblem
  :: GameStuff.Catalog
  -> Set Text
  -> Stuff.Problem
  -> Stuff.Problem
researchProblem catalog known problem@Stuff.Problem{..} =
  problem
    { Stuff.pDiscovery = newDiscovery
    , Stuff.pSolutions = newSolutions
    }
  where
    newDiscovery = pDiscovery + 0.1

    newSolutions =
      if floor newDiscovery > length pSolutions then
        -- XXX: very shaky signal when no solutions can be created
        pSolutions ++ fmap snd (take 1 $ sortOn fst extras)
      else
        pSolutions

    extras = take 1 do
      GameStuff.Problem{tags=problemTags} <- maybeToList $
        Map.lookup pLabel (GameStuff.problems catalog)

      (key, solution) <- Map.toList (GameStuff.solutions catalog)
      guard $ key `notElem` map Stuff.sLabel pSolutions

      let
        GameStuff.Solution{for=solutionTags, ..} = solution
        match = Set.intersection problemTags solutionTags
      guard $ Set.size match > 0
      -- traceShowM (discovery, key, "passed", match)

      pure
        ( discovery
        , Stuff.Solution
            { sLabel     = key
            , sAttack    = attack
            , sDefence   = defence
            , sHitPoints = hp % 1

            , sDiscovery = 0
            , sKnown     = Set.member key known
            , sProblems  = mempty
            }
        )

researchSolution
  :: GameStuff.Catalog
  -> Set Text
  -> Stuff.Solution
  -> Stuff.Solution
researchSolution catalog known solution@Stuff.Solution{..} =
  solution
    { Stuff.sDiscovery = newDiscovery
    , Stuff.sProblems = newProblems
    }
  where
    newDiscovery = sDiscovery + 0.1

    newProblems =
      if floor newDiscovery > length sProblems then
        -- XXX: very shaky signal when no problems can be created
        sProblems ++ fmap snd (take 1 $ sortOn fst extras)
      else
        sProblems

    extras = do
      GameStuff.Solution{tags=solutionTags} <- maybeToList $
        Map.lookup sLabel (GameStuff.solutions catalog)

      (key, problem) <- Map.toList (GameStuff.problems catalog)
      guard $ key `notElem` map Stuff.pLabel sProblems

      let
        GameStuff.Problem{for=problemTags, ..} = problem
        match = Set.intersection solutionTags problemTags
      guard $ Set.size match > 0
      -- traceShowM (discovery, key, "passed", match)

      pure
        ( discovery
        , Stuff.Problem
            { pLabel     = key
            , pAttack    = attack
            , pDefence   = defence
            , pHitPoints = hp % 1

            , pDiscovery = 0
            , pKnown     = Set.member key known
            , pSolutions = mempty
            }
        )

-- * Output

data Observer = Observer
  { focusBoxData :: Stuff.ModelObserver
  }

newObserver :: ResourceT (StageRIO st) Observer
newObserver = Observer
  <$> Stuff.newModelObserver 1

observe
  :: HasVulkan env
  => Process
  -> Observer
  -> RIO env ()
observe Process{..} Observer{..} = do
  context <- ask

  Worker.observeIO_ focusBox focusBoxData $
    Buffer.updateCoherentResize_ context

readObserved
  :: MonadUnliftIO m
  => Observer
  -> m (Situation.NodeInstances)
readObserved Observer{..} = id
  <$> Worker.readObservedIO focusBoxData
