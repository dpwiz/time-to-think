{-# LANGUAGE MultiWayIf #-}

module Stage.Main.World.Situation where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tree (Tree)
import Data.Tree qualified as Tree
import Geomancy.Vec3 qualified as Vec3
import Resource.Buffer qualified as Buffer
import RIO.Lens (ix)
import RIO.List.Partial ((!!))
import RIO.Map qualified as Map
import RIO.Vector.Storable qualified as Storable
import System.Random (randomRIO)
import Text.Printf (printf)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Data.Tree.Layout (symmLayout)
import Engine.Types (StageRIO)
import Engine.UI.Layout qualified as Layout
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (HasVulkan, MonadVulkan, Queues)
import Engine.Worker qualified as Worker
import Render.Font.EvanwSdf.Model qualified as EvanwSdf
import Render.Unlit.Colored.Model qualified as UnlitColored
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.Font.EvanW qualified as Font
import Resource.Model qualified as Model

import Global.Resource.Font qualified as GameFont
import Global.Resource.Combined qualified as Combined
import Stage.Main.World.Stuff qualified as Stuff
import Stage.Main.Resource.Stuff qualified as GameStuff

data Process = Process
  { layout :: TreeProcess

  , taskP     :: Stuff.ModelProcess
  , problemP  :: Stuff.ModelProcess
  , solutionP :: Stuff.ModelProcess

  , labelP :: LabelProcess
  , wireP  :: WireProcess
  }

spawn :: MonadUnliftIO m => Font.Container -> GameStuff.Catalog -> m Process
spawn font catalog = do
  layout <- spawnRoot catalog

  taskNodes <- spawnPicker pickTask layout
  taskP     <- Stuff.spawnNodes taskNodes

  problemNodes <- spawnPicker pickProblem layout
  problemP     <- Stuff.spawnNodes problemNodes

  solutionNodes <- spawnPicker pickSolution layout
  solutionP     <- Stuff.spawnNodes solutionNodes

  labelP <- spawnLabels font layout
  wireP <- spawnWires layout

  pure Process{..}

-- Labels

type LabelProcess = Worker.Merge LabelAttrs
type LabelAttrs = Storable.Vector EvanwSdf.InstanceAttrs

spawnLabels :: MonadUnliftIO m => Font.Container -> TreeProcess -> m LabelProcess
spawnLabels font = Worker.spawnMerge1 (collectLabels font)

collectLabels :: Font.Container -> Tree ((a, SomeStuff), Vec2) -> LabelAttrs
collectLabels font = Storable.concat . concat . toList . fmap makeLabel
  where
    makeLabel ((_ann, stuff), pos) =
      if known then
        [Message.mkAttrs box labelInput]
      else
        concat
          [ [ Message.mkAttrs box discoveryInput | discovery > 0 ]
          , [ Message.mkAttrs box statsInput | discovery > 1 ]
          , [ Message.mkAttrs box labelInput ]
          ]
      where
        (_type, label) = stuffLabel stuff
        (known, discovery, hp :: Float, def, matt) =
          case stuff of
            Task Stuff.Task{..} ->
              ( False
              , tDiscovery
              , realToFrac tHitPoints
              , tDefence
              , Nothing
              )
            Problem Stuff.Problem{..} ->
              ( pKnown
              , pDiscovery
              , realToFrac pHitPoints
              , pDefence
              , Just pAttack
              )
            Solution Stuff.Solution{..} ->
              ( sKnown
              , sDiscovery
              , realToFrac sHitPoints
              , sDefence
              , Just sAttack
              )

        box = Layout.Box pos $ vec2 160 90 - vec2 4 2

        GameFont.Collection{small} = CombinedTextures.fonts Combined.indices

        labelInput = Message.Input
          { inputText         = if known then "(" <> label <> ")" else label
          , inputFontId       = small
          , inputFont         = font
          , inputOrigin       = Layout.Center
          , inputSize         = 16
          , inputColor        = 1 -- vec4 0 0 0 1
          , inputOutline      = vec4 0 0 0 (15/16)
          , inputOutlineWidth = 4/16
          , inputSmoothing    = 1/16
          }

        discoveryInput = labelInput
          { Message.inputOrigin = Layout.RightTop
          , Message.inputText = fromString $ printf "%.1f" discovery
          }

        statsInput = labelInput
          { Message.inputOrigin = Layout.CenterBottom
          , Message.inputText = fromString $
              if
                | discovery > 8 ->
                    case matt of
                      Just att -> printf "A: %d H: %.1f D: %d" att hp def
                      Nothing  -> printf       "H: %.1f D: %d"     hp def
                | discovery > 4 ->
                    case matt of
                      Just att -> printf "A: %d H: %.1f D: ?" att hp
                      Nothing  -> printf       "H: %.1f D: ?"     hp
                | discovery > 2 ->
                    case matt of
                      Just att -> printf "A: %d H:? D:?" att
                      Nothing  ->              "H:? D:?"
                | otherwise ->
                    ""
          }

-- Wires

type WireProcess = Worker.Merge [WireVertex]
type WireVertex = Model.Vertex Vec3.Packed UnlitColored.VertexAttrs

spawnWires :: MonadUnliftIO m => TreeProcess -> m WireProcess
spawnWires = Worker.spawnMerge1 collectWires

collectWires :: Tree (a, Vec2) -> [WireVertex]
collectWires = mconcat . mconcat . makeWires
  where
    makeWires (Tree.Node (_ann, origin) children) =
      case children of
        [] ->
          mempty
        _some ->
          map (connect origin) children : concatMap makeWires children

    connect origin (Tree.Node (_ann, pos) _children) =
      [ Model.Vertex
          { vPosition = Vec3.fromVec2 (origin + vec2 0 45) 0.5
          , vAttrs    = 0.8
          }
      , Model.Vertex
          { vPosition = Vec3.fromVec2 (pos - vec2 0 45) 0.5
          , vAttrs    = 0.5
          }
      ]

--------------------------

getLayoutPath :: Tree a -> [Int] -> Maybe a
getLayoutPath (Tree.Node node children) = \case
  [] ->
    Just node
  cur : next ->
    case children ^? ix cur of
      Nothing ->
        Nothing
      Just found ->
        getLayoutPath found next

getSituationPath :: SomeStuff -> [Int] -> Maybe SomeStuff
getSituationPath someStuff path =
  case path of
    [] ->
      Just someStuff
    cur : next ->
      case someStuff of
        Task Stuff.Task{tProblems} ->
          case tProblems ^? ix cur of
            Nothing ->
              Nothing
            Just found ->
              getSituationPath (Problem found) next

        Problem Stuff.Problem{pSolutions} ->
          case pSolutions ^? ix cur of
            Nothing ->
              Nothing
            Just found ->
              getSituationPath (Solution found) next

        Solution Stuff.Solution{sProblems} ->
          case sProblems ^? ix cur of
            Nothing ->
              Nothing
            Just found ->
              getSituationPath (Problem found) next

toTree :: Poop k a -> Tree (a, SomeStuff)
toTree = \case
  AtRoot task ann rootProbs ->
    Tree.Node (ann, Task task) (map toTree rootProbs)

  AtProblem prob ann solutions ->
    Tree.Node (ann, Problem prob) (map toTree solutions)

  AtSolution sol ann problems ->
    Tree.Node (ann, Solution sol) (map toTree problems)

data SomeStuff
  = Task     Stuff.Task
  | Problem  Stuff.Problem
  | Solution Stuff.Solution
  deriving (Show)

stuffLabel :: SomeStuff -> (Text, Text)
stuffLabel = \case
  Task     Stuff.Task{tLabel}     -> ("task",     tLabel)
  Problem  Stuff.Problem{pLabel}  -> ("problem",  pLabel)
  Solution Stuff.Solution{sLabel} -> ("solution", sLabel)

data T = R | P | S

data Poop (k :: T) a where
  AtRoot     :: Stuff.Task     -> a -> [Poop 'P a] -> Poop 'R a
  AtProblem  :: Stuff.Problem  -> a -> [Poop 'S a] -> Poop 'P a
  AtSolution :: Stuff.Solution -> a -> [Poop 'P a] -> Poop 'S a

deriving instance
  (Show a) => Show (Poop k a)

type TreeProcess = Worker.Cell Stuff.Task (Tree (([Int], SomeStuff), Vec2))
type TreeLayout = Tree (([Int], SomeStuff), Vec2)

spawnRoot
  :: MonadUnliftIO m
  => GameStuff.Catalog
  -> m TreeProcess
spawnRoot GameStuff.Catalog{tasks} = do
  catalogTask <-
    if Map.null tasks then
      pure Nothing
    else do
      pick <- randomRIO (0, Map.size tasks - 1)
      let key = Map.keys tasks !! pick
      pure . fmap (key,) $ Map.lookup key tasks

  let
    initialTask = case catalogTask of
      Nothing ->
        Stuff.Task
          { tLabel     = "play game"
          , tHitPoints = 1
          , tDefence   = 1
          , tDiscovery = 0
          , tProblems  =
              [ Stuff.Problem
                  { pLabel     = "no tasks in catalog"
                  , pHitPoints = 1
                  , pAttack    = 5
                  , pDefence   = 1

                  , pDiscovery = 0
                  , pKnown     = True
                  , pSolutions = []
                  }
              ]
          }
      Just (taskName, GameStuff.Task{..}) ->
        Stuff.Task
          { tLabel     = taskName
          , tHitPoints = hp % 1
          , tDefence   = defence

          , tDiscovery = 0
          , tProblems = []
          }

  Worker.spawnCell
    (symmLayout 0 . toTree . unfoldTask)
    initialTask

unfoldTask :: Stuff.Task -> Poop 'R [Int]
unfoldTask task@Stuff.Task{tProblems} =
  AtRoot task [] $
    map
      (uncurry $ unfoldProblem [])
      (zip [0 ::Int ..] tProblems)

unfoldProblem :: [Int] -> Int -> Stuff.Problem -> Poop 'P [Int]
unfoldProblem ancestors pathIx problem =
  AtProblem problem (reverse path) $
    map
      (uncurry $ unfoldSolution path)
      (zip [0 ::Int ..] $ Stuff.pSolutions problem)
  where
    path = pathIx : ancestors

unfoldSolution :: [Int] -> Int -> Stuff.Solution -> Poop 'S [Int]
unfoldSolution ancestors pathIx solution =
  AtSolution solution (reverse path) $
    map
      (uncurry $ unfoldProblem path)
      (zip [0 ::Int ..] $ Stuff.sProblems solution)
  where
    path = pathIx : ancestors

type LayoutTree = Tree (([Int], SomeStuff), Vec2)

spawnPicker
  :: ( MonadUnliftIO m
     , Worker.HasOutput task
     , Worker.GetOutput task ~ LayoutTree
     )
  => Picker
  -> task
  -> m (Worker.Merge [Layout.Box])
spawnPicker picker = Worker.spawnMerge1 $ foldr picker []

type Picker = (([Int], SomeStuff), Vec2) -> [Layout.Box] -> [Layout.Box]

pickTask :: Picker
pickTask ((_ann, ss), pos) acc =
  case ss of
    Task{} ->
      Layout.Box pos (vec2 160 90) : acc
    _ ->
      acc

pickProblem :: Picker
pickProblem ((_ann, ss), pos) acc =
  case ss of
    Problem{} ->
      Layout.Box pos (vec2 160 90) : acc
    _ ->
      acc

pickSolution :: Picker
pickSolution ((_ann, ss), pos) acc =
  case ss of
    Solution{} ->
      Layout.Box pos (vec2 160 90) : acc
    _ ->
      acc

data Observer = Observer
  { taskData     :: Stuff.ModelObserver
  , problemData  :: Stuff.ModelObserver
  , solutionData :: Stuff.ModelObserver
  , labelData    :: Message.Observer
  , wireData     :: WireObserver
  }

type NodeInstances = Buffer.Allocated 'Buffer.Coherent Transform
type LabelInstances = Buffer.Allocated 'Buffer.Coherent EvanwSdf.InstanceAttrs

newObserver :: ResourceT (StageRIO st) Observer
newObserver = Observer
  <$> Stuff.newModelObserver 8
  <*> Stuff.newModelObserver 128
  <*> Stuff.newModelObserver 128
  <*> Message.newObserver 2048
  <*> newWireObserver 4096

observe
  :: HasVulkan env
  => Process
  -> Observer
  -> RIO env ()
observe Process{..} Observer{..} = do
  context <- ask

  Worker.observeIO_ taskP taskData $
    Buffer.updateCoherentResize_ context

  Worker.observeIO_ problemP problemData $
    Buffer.updateCoherentResize_ context

  Worker.observeIO_ solutionP solutionData $
    Buffer.updateCoherentResize_ context

  Message.observe labelP labelData

  Worker.observeIO_ wireP wireData \oldModel newVertices ->
    Model.updateCoherent context newVertices oldModel

readObserved
  :: MonadUnliftIO m
  => Observer
  -> m (NodeInstances, NodeInstances, NodeInstances, Message.Buffer, WireModel)
readObserved Observer{..} = (,,,,)
  <$> Worker.readObservedIO taskData
  <*> Worker.readObservedIO problemData
  <*> Worker.readObservedIO solutionData
  <*> Worker.readObservedIO labelData
  <*> Worker.readObservedIO wireData

type WireModel = Model.Indexed 'Buffer.Coherent Vec3.Packed UnlitColored.VertexAttrs
type WireObserver = Worker.ObserverIO WireModel

newWireObserver :: Int -> ResourceT (StageRIO st) WireObserver
newWireObserver initialSize = do
  context <- ask
  wireData <- Model.createCoherentEmpty context initialSize

  observer <- Worker.newObserverIO wireData

  _transient <- Resource.register $
    Worker.readObservedIO observer >>=
      Model.destroyIndexed context

  pure observer

-- * Hand-offs

data Snapshot = Snapshot
  { situationTree   :: Worker.GetInput TreeProcess
  , situationLayout :: Worker.GetOutput TreeProcess
  , taskOut         :: Worker.GetOutput Stuff.ModelProcess
  , problemOut      :: Worker.GetOutput Stuff.ModelProcess
  , solutionOut     :: Worker.GetOutput Stuff.ModelProcess
  , labelOut        :: Worker.GetOutput LabelProcess
  , wireOut         :: Worker.GetOutput WireProcess
  }

takeSnapshot :: Process -> STM Snapshot
takeSnapshot Process{..} = do
  situationTree   <- fmap Worker.vData . readTVar $ Worker.getInput layout
  situationLayout <- fmap Worker.vData . readTVar $ Worker.getOutput layout
  taskOut         <- fmap Worker.vData . readTVar $ Worker.getOutput taskP
  problemOut      <- fmap Worker.vData . readTVar $ Worker.getOutput problemP
  solutionOut     <- fmap Worker.vData . readTVar $ Worker.getOutput solutionP
  labelOut        <- fmap Worker.vData . readTVar $ Worker.getOutput labelP
  wireOut         <- fmap Worker.vData . readTVar $ Worker.getOutput wireP
  pure Snapshot{..}

data SnapshotData = SnapshotData
  { sdTasks     :: Buffer.Allocated 'Buffer.Coherent Transform
  , sdProblems  :: Buffer.Allocated 'Buffer.Coherent Transform
  , sdSolutions :: Buffer.Allocated 'Buffer.Coherent Transform

  , sdLabels :: Buffer.Allocated 'Buffer.Coherent EvanwSdf.InstanceAttrs

  , sdWires :: Model.Indexed 'Buffer.Coherent Vec3.Packed UnlitColored.VertexAttrs
  }

snapshotData
  :: (MonadVulkan env m, Resource.MonadResource m)
  => Queues Vk.CommandPool
  -> Snapshot
  -> m (Resource.ReleaseKey, SnapshotData)
snapshotData _pools Snapshot{..} = do
  context <- ask

  sdTasks <- Buffer.createCoherent
    context
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    taskOut

  sdProblems <- Buffer.createCoherent
    context
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    problemOut

  sdSolutions <- Buffer.createCoherent
    context
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    solutionOut

  sdLabels <- Buffer.createCoherent
    context
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    labelOut

  emptyWires <- Model.createCoherentEmpty context 1024
  sdWires <- Model.updateCoherent context wireOut emptyWires

  key <- Resource.register do
    Buffer.destroy context sdTasks
    Buffer.destroy context sdProblems
    Buffer.destroy context sdSolutions
    Buffer.destroy context sdLabels
    Model.destroyIndexed context sdWires

  pure (key, SnapshotData{..})
