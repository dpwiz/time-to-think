module Stage.Main.World.Scene where

import RIO

import Geomancy (Vec2, withVec2)
import Geomancy.Transform qualified as Transform

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene(..), emptyScene)

type Process = Worker.Merge Scene

type View = (Vec2, Float)
type ViewProcess = Worker.Var View

mkScene :: Camera.Projection -> View -> Scene
mkScene Camera.Projection{..} (pan, zoom) =
  emptyScene
    { sceneProjection    = projectionOrthoUI
    , sceneInvProjection = Transform.inverse projectionOrthoUI -- FIXME: move to cell output
    , sceneView          = sceneView
    , sceneInvView       = Transform.inverse sceneView
    }
  where
    sceneView = mconcat
      [ withVec2 (-pan) \panX panY ->
          Transform.translate panX panY 0
      , Transform.scale zoom
      ]

mkSceneUi :: Camera.Projection -> Scene
mkSceneUi Camera.Projection{..} =
  emptyScene
    { sceneProjection    = projectionOrthoUI
    , sceneInvProjection = Transform.inverse projectionOrthoUI -- FIXME: move to cell output
    , sceneView          = mempty
    , sceneInvView       = mempty
    }
