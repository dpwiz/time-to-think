module Stage.Main.World.Stuff where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Types (StageRIO)
import Engine.Vulkan.Types (HasVulkan)
import Geomancy (Transform)
import Geomancy.Transform qualified as Transform
import Resource.Buffer qualified as Buffer
import RIO.Lens (ix)
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Engine.Worker qualified as Worker
import Engine.UI.Layout qualified as Layout

-- * Models

type ModelProcess = Worker.Merge (Storable.Vector Transform)

spawnNodes
  :: ( MonadUnliftIO m
     , Worker.HasOutput nodes
     , Worker.GetOutput nodes ~ [Layout.Box]
     )
  => nodes
  -> m ModelProcess
spawnNodes = Worker.spawnMerge1 processNodes

processNodes :: [Layout.Box] -> Storable.Vector Transform
processNodes = Storable.fromList . map mkTransform
  where
    mkTransform box =
      Transform.translate 0 0 0.1 <>
      Layout.boxTransformAbs box

type ModelObserver = Worker.ObserverIO (Buffer.Allocated 'Buffer.Coherent Transform)

newModelObserver :: Int -> ResourceT (StageRIO st) ModelObserver
newModelObserver initialSize = do
  context <- ask

  (_transient, initialData) <-
    Buffer.allocateCoherent
      context
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      initialSize
      mempty

  Worker.newObserverIO initialData

observeModel :: HasVulkan env => ModelProcess -> ModelObserver -> RIO env ()
observeModel process observer = do
  context <- ask
  Worker.observeIO_ process observer $
    Buffer.updateCoherentResize_ context

-- * Data

data Task = Task
  { tLabel     :: Text

  , tHitPoints :: Rational
  , tDefence   :: Integer

  , tDiscovery :: Float

  , tProblems  :: [Problem]
  }
  deriving (Show)

taskProblems :: Lens' Task [Problem]
taskProblems = lens tProblems \task new -> task{tProblems = new}

data Problem = Problem
  { pLabel     :: Text

  , pAttack    :: Integer
  , pDefence   :: Integer
  , pHitPoints :: Rational

  , pDiscovery :: Float
  , pKnown     :: Bool

  , pSolutions :: [Solution]
  }
  deriving (Show)

problemSolutions :: Lens' Problem [Solution]
problemSolutions = lens pSolutions \problem new -> problem{pSolutions = new}

modifyProblem
  :: Either (Problem -> Problem) (Solution -> Solution)
  -> [Int]
  -> Problem
  -> Problem
modifyProblem ef path problem =
  case path of
    [] ->
      case ef of
        Left pf ->
          pf problem
        Right _sf ->
          problem
    here : deeper ->
      problem &
        problemSolutions . ix here %~ modifySolution ef deeper

data Solution = Solution
  { sLabel    :: Text

  , sAttack    :: Integer
  , sDefence   :: Integer
  , sHitPoints :: Rational

  , sDiscovery :: Float
  , sKnown     :: Bool

  , sProblems :: [Problem]
  }
  deriving (Show)

solutionProblems :: Lens' Solution [Problem]
solutionProblems = lens sProblems \solution new -> solution{sProblems = new}

modifySolution
  :: Either (Problem -> Problem) (Solution -> Solution)
  -> [Int]
  -> Solution
  -> Solution
modifySolution ef path solution =
  case path of
    [] ->
      case ef of
        Left _pf ->
          solution
        Right sf ->
          sf solution
    here : deeper ->
      solution &
        solutionProblems . ix here %~ modifyProblem ef deeper
