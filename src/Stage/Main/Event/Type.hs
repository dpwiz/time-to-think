module Stage.Main.Event.Type
  ( Event(..)
  ) where

import RIO

import Stage.Main.World.Situation qualified as Situation

data Event
  = StopThinking
  | StartThinking [Int] Situation.SomeStuff
  | RunSim
  deriving (Show, Generic)
