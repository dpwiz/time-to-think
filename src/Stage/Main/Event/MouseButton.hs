{-# OPTIONS_GHC -Wwarn=deprecations #-}

module Stage.Main.Event.MouseButton
  ( clickHandler
  ) where

import RIO

import RIO.State (gets)
import Geomancy (vec2)
-- import RIO.NonEmpty.Partial qualified as NonEmpty (fromList)

import Engine.Events.MouseButton (ClickHandler)
import Engine.Events.Sink (Sink(..))
import Engine.UI.Layout qualified as Layout
import Engine.Window.MouseButton (MouseButton(..), MouseButtonState(..))
import Engine.Worker qualified as Worker

import Stage.Main.Event.Type (Event)
import Stage.Main.Event.Type qualified as Event
import Stage.Main.Types (RunState(..))
-- import Stage.Main.UI qualified as UI
import Stage.Main.World.Situation qualified as Situation

clickHandler :: ClickHandler Event RunState
clickHandler (Sink signal) uiPos buttonEvent = do
  -- ui <- gets rsUI
  -- uiDisplay <- Worker.getOutputData (UI.display ui)

  (pan, _zoom) <- gets rsViewP >>=
    Worker.getInputData

  let scenePos = uiPos + pan

  situation <- gets rsSituation
  layout <- Worker.getOutputData $ Situation.layout situation

  -- thinking <- gets rsThinking

  case buttonEvent of
    (_mods, MouseButtonState'Released, MouseButton'1) ->
      signal Event.StopThinking

    (_mods, MouseButtonState'Pressed, MouseButton'1) -> do
      let
        clicked = listToMaybe do
          (node, pos) <- toList layout
          guard $
            Layout.pointInBox scenePos (Layout.Box pos $ vec2 160 90)
          pure node
      for_ clicked \(path, stuff) -> do
        signal $ Event.StartThinking path stuff

    (_mods, MouseButtonState'Pressed, MouseButton'2) ->
      traceM "TODO: pan start"
    (_mods, MouseButtonState'Released, MouseButton'2) ->
      traceM "TODO: pan stop"

    (_mods, MouseButtonState'Pressed, MouseButton'3) ->
      signal Event.RunSim

    _ ->
      logInfo $ "MouseButton event: " <> displayShow (uiPos, buttonEvent)
