module Stage.Main.Event.Sink
  ( handleEvent
  ) where

import RIO

import RIO.State (get, gets)

import Engine.StageSwitch (trySwitchStage)
import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker

import Stage.Main.Event.Type (Event(..))
import Stage.Main.Types (RunState(..))
import Stage.Main.UI qualified as UI
import Stage.Main.World.Situation qualified as Situation
import Stage.Main.World.Thinking qualified as Thinking

import Stage.Sim.Setup qualified as Sim

handleEvent :: Event -> StageRIO RunState ()
handleEvent = \case
  StartThinking path someStuff -> do
    let (cls, label) = Situation.stuffLabel someStuff

    ui <- gets rsUI
    thinking <- gets rsThinking

    atomically do
      UI.postMessage_ ui $
        "Started thinking about " <> cls <> ": " <> label
      Worker.pushInputSTM (Thinking.focusPath thinking) (const $ Just path)
      writeTVar (Worker.tActive $ Thinking.thinkTimer thinking) True
      writeTVar (Worker.tActive $ Thinking.focusTimer thinking) True

  StopThinking -> do
    thinking <- gets rsThinking

    thought <- atomically do
      Worker.pushInputSTM
        (Thinking.focusPath thinking)
        (const Nothing)

      writeTVar (Worker.tActive $ Thinking.thinkTimer thinking) False
      writeTVar (Worker.tActive $ Thinking.focusTimer thinking) False

      fmap Worker.vData . readTVar $
        (Worker.getOutput $ Thinking.focusNode thinking)

    case thought of
      Nothing ->
        -- XXX: StopThinking issued blindly on mouse release
        pure ()

      Just (_pos, someStuff) -> do
        let (cls, label) = Situation.stuffLabel someStuff
        logDebug $ "Stopped thinking about " <> display cls <> ": " <> display label
        -- ui <- gets rsUI
        -- atomically . UI.postMessage_ ui $
        --   "Stopped thinking about " <> cls <> ": " <> label

  RunSim -> do
    RunState{..} <- get

    -- void $! atomically do
    --   snapshot <- Situation.takeSnapshot rsSituation
    --   trySwitchStage rsNextStage . Engine.Replace $
    --     Sim.stackStage rsAssets snapshot

    snapshot <- atomically $ Situation.takeSnapshot rsSituation
    void $! trySwitchStage . Engine.Replace $
      Sim.stackStage rsAssets snapshot
