{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO

import RIO.State (gets)
import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa

import Global.Resource.Assets (aModels)
import Stage.Main.Types (FrameResources(..), RunState(..))
import Global.Resource.Model qualified as Model
import Stage.Main.World.Situation qualified as Situation
import Stage.Main.World.Thinking qualified as Thinking
import Stage.Main.UI qualified as UI

updateBuffers
  :: RunState
  -> FrameResources
  -> Basic.StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Set0.observe rsSceneP frScene
  Set0.observe rsSceneUiP frSceneUi

  Situation.observe rsSituation frSituation
  Thinking.observe rsThinking frThinking
  UI.observe rsUI frUI

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> Basic.StageFrameRIO FrameResources RunState ()
recordCommands cb FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask
  let Basic.Pipelines{..} = fPipelines

  Model.Collection{..} <- gets $ aModels . rsAssets
  (tasks, problems, solutions, labels, wires) <- Situation.readObserved frSituation
  foci <- Thinking.readObserved frThinking

  (messages, messageBg, thinkingBg) <- UI.readObserved frUI

  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Set0.withBoundSet0 frScene pWireframe cb do
      Pipeline.bind cb pWireframe $
        Draw.indexed cb wires frZeroTransform

      Pipeline.bind cb pUnlitColored do
        Draw.indexed cb task tasks
        Draw.indexed cb problem problems
        Draw.indexed cb solution solutions
        Draw.indexed cb focus foci

      Pipeline.bind cb pEvanwSdf $
        Draw.quads cb labels

    Set0.withBoundSet0 frSceneUi pUnlitColored cb do
      Pipeline.bind cb pUnlitColored do
        Draw.indexed cb shroud messageBg
        Draw.indexed cb shroud thinkingBg

      Pipeline.bind cb pEvanwSdf $
        traverse_ (Draw.quads cb) messages

    -- ImGui.draw dear cb
