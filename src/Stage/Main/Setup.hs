{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Setup
  ( stackStage
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
import Engine.Events.CursorPos qualified as CursorPos
import Engine.Events.MouseButton qualified as MouseButton
import Engine.Types (StackStage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.Samplers qualified as Samplers
import Resource.Buffer qualified as Buffer

import Global.Resource.Assets (Assets(..))
import Global.Resource.Combined qualified as Combined
import Global.Resource.CubeMap qualified as CubeMap
import Global.Resource.Font qualified as GlobalFont
import Stage.Main.Event.MouseButton qualified as MouseButton
import Stage.Main.Event.Sink (handleEvent)
import Stage.Main.Render qualified as Render
import Stage.Main.Types (FrameResources(..), RunState(..), Stage)
import Stage.Main.UI qualified as UI
import Stage.Main.World.Scene qualified as Scene
import Stage.Main.World.Situation qualified as Situation
import Stage.Main.World.Thinking qualified as Thinking

stackStage :: Assets -> StackStage
stackStage = StackStage . stage

stage :: Assets -> Stage
stage assets = Engine.Stage
  { sTitle = "Main"

  , sAllocateP  = allocatePipelines
  , sInitialRS  = initialRunState assets
  , sInitialRR  = initialFrameResources assets
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sAfterLoop = afterLoop
  }
  where
    allocatePipelines swapchain rps = do
      (_, samplers) <- Samplers.allocate swapchain
      let
        sceneBinds =
          Scene.mkBindings
            samplers
            Combined.sources
            CubeMap.sources
            0 -- no shadows
      Basic.allocatePipelines sceneBinds swapchain rps

    beforeLoop = do
      cursorWindow <- gets rsCursorPos
      cursorCentered <- gets rsCursorP

      (key, sink) <- Events.spawn
        handleEvent
        [ CursorPos.callback cursorWindow
        , MouseButton.callback cursorCentered MouseButton.clickHandler
        ]

      modify' \rs -> rs
        { rsEvents = Just sink
        }

      -- ImGui.beforeLoop True
      pure key

    afterLoop key = do
      -- ImGui.afterLoop
      Resource.release key

initialRunState :: Assets -> StageRIO env (Resource.ReleaseKey, RunState)
initialRunState rsAssets = do
  rsProjectionP <- Engine.getScreenP
  rsCursorPos <- Worker.newVar 0
  (cursorKey, rsCursorP) <- Worker.registered $
    Worker.spawnMerge2
      (\Camera.ProjectionInput{projectionScreen} (WithVec2 windowX windowY) ->
        let
          Vk.Extent2D{width, height} = projectionScreen
        in
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      (Worker.getInput rsProjectionP)
      rsCursorPos

  (screenKey, screenBoxP) <- Worker.registered Layout.trackScreen

  rsViewP <- Worker.newVar (0, 1)

  (sceneKey, rsSceneP) <- Worker.registered $
    Worker.spawnMerge2 Scene.mkScene rsProjectionP rsViewP

  (sceneUiKey, rsSceneUiP) <- Worker.registered $
    Worker.spawnMerge1 Scene.mkSceneUi rsProjectionP

  (uiKey, rsUI) <- UI.spawn rsAssets screenBoxP rsViewP

  rsSituation <- Situation.spawn (GlobalFont.small $ aFonts rsAssets) (aCatalog rsAssets)
  -- TODO: situationKey

  rsThinking <- Thinking.spawn rsUI rsSituation (aCatalog rsAssets) rsViewP

  releaseKeys <- Resource.register $
    traverse_ @[] Resource.release
      [ cursorKey
      , screenKey
      , sceneKey
      , sceneUiKey
      , uiKey
      ]

  let rsEvents = Nothing

  pure (releaseKeys, RunState{..})

initialFrameResources
  :: Assets
  -> Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Basic.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources Assets{..} pools _passes pipelines = do
  context <- ask

  frScene <- Scene.allocate
    (Basic.getSceneLayout pipelines)
    aTextures
    Nothing
    Nothing
    mempty
    Nothing

  frSceneUi <- Scene.allocate
    (Basic.getSceneLayout pipelines)
    aTextures
    Nothing
    Nothing
    mempty
    Nothing

  frZeroTransform <- Buffer.createStaged
    context
    pools
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    [mempty]
  void . Resource.register $
    Buffer.destroy context frZeroTransform

  frSituation <- Situation.newObserver
  frThinking <- Thinking.newObserver

  ui <- gets rsUI
  frUI <- UI.newObserver ui

  pure FrameResources{..}
