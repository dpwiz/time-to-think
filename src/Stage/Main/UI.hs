module Stage.Main.UI
  ( UI(..)
  , spawn

  , postMessage_
  , postMessageRare
  , postMessage

  , Observer(..)
  , newObserver
  , observe
  , readObserved
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Geomancy (Transform, Vec4, vec2, vec4)
import Resource.Buffer qualified as Buffer
import Resource.Combined.Textures qualified as CombinedTextures
import RIO.List (unzip3)
import RIO.Vector.Storable qualified as Storable
import Text.Printf (printf)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Global.Resource.Assets (Assets(..))
import Global.Resource.Combined qualified as Combined
import Global.Resource.Font qualified as GlobalFont
import Stage.Main.Resource.Stuff qualified as GameStuff
import Stage.Main.World.Scene qualified as Scene

data UI = UI
  { messageLines   :: [Worker.Var (Float, Vec4, Text)]
  , messageWorkers :: [Message.Process]
  , messageBgP     :: Worker.Merge (Storable.Vector Transform)

  , thinkingTime :: Worker.Var Float
  , thinkingP    :: Message.Process
  , thinkingBgP  :: Worker.Merge (Storable.Vector Transform)
  }

spawn
  :: Assets
  -> Layout.BoxProcess
  -> Scene.ViewProcess
  -> Engine.StageRIO env (Resource.ReleaseKey, UI)
spawn assets screenP _viewVar = do
  (messageBoxKey, messageBoxP) <- Worker.registered $
    Layout.fitPlaceAbs
      Layout.CenterBottom
      (vec2 1024 $ sum lineSizes)
      screenP

  lineBoxes <- Layout.splitsRelStatic Layout.sharePadsV messageBoxP lineSizes
  lineBoxesKey <- Worker.registerCollection lineBoxes

  messageLines <- for lineSizes \lineSize ->
    Worker.newVar (lineSize, 1, "")

  messages <- for (zip lineBoxes messageLines) \(lineBoxP, messageVar) ->
    Message.spawnFromR lineBoxP messageVar \(size, color, text) ->
      messageLine size color text
  let (inputKeys, messageKeys, messageWorkers) = unzip3 messages

  (thinkingBoxKey, thinkingBoxP) <- Worker.registered $
    Layout.fitPlaceAbs Layout.CenterTop (vec2 480 42) screenP

  thinkingTime <- Worker.newVar 0.0
  (thinkingInputKey, thinkingKey, thinkingP) <-
    Message.spawnFromR thinkingBoxP thinkingTime \ft ->
      let
        msg =
          if ft < 0.5 then
            "Start thinking!"
          else
            fromString $ printf "Thinking time: %.2f" ft
      in
        (messageLine 32 1 msg)
          { Message.inputOrigin = Layout.Center
          }

  (thinkingBgKey, thinkingBgP) <- Worker.registered $
    Worker.spawnMerge1 (Storable.singleton . Layout.boxTransformAbs) thinkingBoxP

  (messageBgKey, messageBgP) <- Worker.registered $
    Worker.spawnMerge1 (Storable.singleton . Layout.boxTransformAbs) messageBoxP

  key <- Resource.register $ traverse_ Resource.release $ mconcat
    [ [ messageBoxKey
      , lineBoxesKey
      , messageBgKey

      , thinkingBoxKey, thinkingInputKey, thinkingKey
      , thinkingBgKey
      ]
    , inputKeys
    , messageKeys
    ]

  pure (key, UI{..})
  where
    messageLine size color text = Message.Input
      { inputText         = text
      , inputOrigin       = Layout.CenterBottom
      , inputSize         = size
      , inputColor        = color
      , inputFont         = GlobalFont.small (aFonts assets)
      , inputFontId       = GlobalFont.small $ CombinedTextures.fonts Combined.indices
      , inputOutline      = vec4 0 0 0 1
      , inputOutlineWidth = 2/32
      , inputSmoothing    = 4/32
      }

lineSizes :: [Float]
lineSizes = replicate 5 22

numLines :: Int
numLines = length lineSizes

-- * Updates

postMessage :: UI -> Float -> Vec4 -> Text -> STM ()
postMessage UI{..} size color text = do
  current <- traverse (fmap Worker.vData . readTVar) messageLines
  let new = take numLines $ (size, color, text) : map fade current
  for_ (zip messageLines new) \(var, input) ->
    Worker.pushInputSTM var (const input)
  where
    fade (s, c, t) = (max 16 $ s - 1, c * 0.9, t)

postMessageRare :: UI -> GameStuff.Rarity -> Text -> STM ()
postMessageRare ui rarity text =
  postMessage ui (fromIntegral $ 16 + fromEnum rarity) (rareColor rarity) text

postMessage_ :: UI -> Text -> STM ()
postMessage_ ui = postMessage ui 16 0.9

rareColor :: GameStuff.Rarity -> Vec4
rareColor = \case
  GameStuff.Common    -> rgbI 202 202 202
  GameStuff.Uncommon  -> rgbI 128 207  63
  GameStuff.Rare      -> rgbI  47 213   1
  GameStuff.Epic      -> rgbI 189  63 250
  GameStuff.Legendary -> rgbI 253 174  83
  GameStuff.Mythic    -> rgbI 252 221 121
  where
    rgbI r g b = vec4 (r/255) (g/255) (b/255) 1

-- * Output

data Observer = Observer
  { messages   :: [Message.Observer]
  , messagesBg :: BgObserver

  , thinking   :: Message.Observer
  , thinkingBg :: BgObserver
  }

type BgBuffer = Buffer.Allocated 'Buffer.Coherent Transform
type BgObserver = Worker.ObserverIO BgBuffer

newObserver :: UI -> ResourceT (Engine.StageRIO st) Observer
newObserver UI{..} = do
  messages <- traverse (const $ Message.newObserver 256) messageWorkers
  messagesBg <- newBgObserver

  thinking <- Message.newObserver 256
  thinkingBg <- newBgObserver

  pure Observer{..}

newBgObserver :: ResourceT (Engine.StageRIO st) BgObserver
newBgObserver = do
  context <- ask

  (_transient, initialData) <-
    Buffer.allocateCoherent
      context
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      1
      mempty

  Worker.newObserverIO initialData


observe
  :: ( HasVulkan env
     )
  => UI
  -> Observer
  -> RIO env ()
observe UI{..} Observer{..} = do
  traverse_ (uncurry Message.observe) $
    zip messageWorkers messages
  Message.observe thinkingP thinking

  context <- ask
  Worker.observeIO_ messageBgP messagesBg $
    Buffer.updateCoherentResize_ context
  Worker.observeIO_ thinkingBgP thinkingBg $
    Buffer.updateCoherentResize_ context

readObserved
  :: MonadUnliftIO m
  => Observer
  -> m ([Message.Buffer], BgBuffer, BgBuffer)
readObserved Observer{..} = (,,)
  <$> traverse Worker.readObservedIO (thinking : messages)
  <*> Worker.readObservedIO thinkingBg
  <*> Worker.readObservedIO messagesBg
