module Stage.Main.Resource.Stuff
  ( load
  , Catalog(..)

  , Task(..)
  , Problem(..)
  , Solution(..)

  , Rarity(..)
  , Relation(..)
  ) where

import RIO
import Data.Aeson

import Data.Yaml qualified as Yaml

load :: MonadIO m => FilePath -> m Catalog
load = Yaml.decodeFileThrow

-- * Catalog/setting/mod

data Catalog = Catalog
  { tasks     :: Map Text Task
  , problems  :: Map Text Problem
  , solutions :: Map Text Solution
  }
  deriving (Show, Generic)

instance FromJSON Catalog

-- * Major components

data Task = Task
  { tags    :: Set Text
  , defence :: Integer
  , hp      :: Integer
  }
  deriving (Eq, Ord, Show, Generic)

instance FromJSON Task

data Problem = Problem
  { for       :: Set Text
  , tags      :: Set Text
  , occurence :: Rarity
  , discovery :: Rarity
  , attack    :: Integer
  , defence   :: Integer
  , hp        :: Integer
  }
  deriving (Eq, Ord, Show, Generic)

instance FromJSON Problem

data Solution = Solution
  { for       :: Set Text
  , tags      :: Set Text
  , discovery :: Rarity
  , attack    :: Integer
  , defence   :: Integer
  , hp        :: Integer
  , strong    :: Maybe (Set Relation)
  , weak      :: Maybe (Set Relation)
  }
  deriving (Eq, Ord, Show, Generic)

instance FromJSON Solution

-- Misc

data Rarity
  = Common
  | Uncommon
  | Rare
  | Epic
  | Legendary
  | Mythic
  deriving (Eq, Ord, Show, Bounded, Enum, Generic)

instance FromJSON Rarity where
  parseJSON = withText "Rarity" \case
    "common"    -> pure Common
    "uncommon"  -> pure Uncommon
    "rare"      -> pure Rare
    "epic"      -> pure Epic
    "legendary" -> pure Legendary
    "mythic"    -> pure Mythic
    unexpected  -> fail $ "unexpected rarity: " <> show unexpected

data Relation
  = Exact Text
  | Tag Text
  deriving (Eq, Ord, Show, Generic)

instance FromJSON Relation where
  parseJSON v = asum
    [ parseExact v
    , parseTag v
    , unexpected
    ]
    where
      parseExact = withObject "Exact" \o -> Exact
        <$> o .: "exact"

      parseTag = withObject "Tag" \o -> Tag
        <$> o .: "tag"

      unexpected =
        fail "relation should be wrapped in 'exact' or 'tag'"
