module RIO.Local
  ( module RIO
  , module RIO.Local
  , module RE
  ) where

import RIO

import Data.Bits as RE (Bits, zeroBits, (.|.), (.&.))
import Data.List as RE (sort)
import Data.Ratio as RE ((%))
import Geomancy as RE
import RIO.FilePath as RE ((</>))
import RIO.State as RE (get, gets, modify')
import Vulkan.CStruct.Extends as RE (SomeStruct(..))
import Vulkan.NamedType as RE ((:::))
import Vulkan.Zero as RE (Zero(..))

τ :: Float
τ = 2 * pi
