{-# LANGUAGE OverloadedLists #-}

module Global.Resource.Assets
  ( Assets(..)
  , load
  ) where

import RIO.Local

import Control.Monad.Trans.Resource qualified as Resource
import UnliftIO.Resource (MonadResource)
import Vulkan.Core10 qualified as Vk
import Data.Yaml.Config (ignoreEnv, loadYamlSettings)
import RIO.Directory (getDirectoryContents, doesFileExist)
import RIO.FilePath (takeExtension)
import Engine.Vulkan.Types (MonadVulkan, Queues)
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.CommandBuffer (withPools)
import Resource.Font qualified as Font
import Resource.Texture qualified as Texture
import Resource.Texture.Ktx1 qualified as Ktx1

import Global.Resource.Combined qualified as Combined
import Global.Resource.CubeMap qualified as CubeMap
import Global.Resource.Font (FontCollection)
import Global.Resource.Font qualified as GameFont
import Global.Resource.Model qualified as GameModel
import Global.Resource.Texture qualified as Textures
import Stage.Main.Resource.Stuff qualified as GameStuff

data Assets = Assets
  { aKey       :: Resource.ReleaseKey
  , aFonts     :: FontCollection
  , aModels    :: GameModel.Collection
  , aTextures  :: Combined.Textures
  , aCubeMaps  :: CubeMap.Textures
  -- , aMaterials :: MaterialCollection
  -- , aSounds    :: Sound.Collection Opus.Source
  , aCatalog :: GameStuff.Catalog
  }

load
  :: ( MonadVulkan env m
     , HasLogFunc env
     , MonadThrow m
     , MonadResource m
     )
  => FilePath
  -> (Text -> m ())
  -> m Assets
load catalogPath update' = do
  is <- Resource.createInternalState
  aKey <- Resource.register $ Resource.closeInternalState is
  let update = lift . update'
  flip Resource.runInternalState is do
    -- update "Loading sounds"
    -- (_soundDeviceKey, soundDevice) <- SoundDevice.allocate
    -- (_soundsKey, !aSounds) <- SoundSource.allocateCollectionWith
    --   (Opus.load soundDevice)
    --   Sound.configs
    -- SoundSource.play (Sound.bg aSounds)
    -- let
    --   update msg = do
    --     SoundSource.play (Sound.detect sounds)
    --     update' msg

    update "Loading catalogs"
    dir <- getDirectoryContents catalogPath
    yamls <- for dir \name -> do
      let fullPath = catalogPath </> name
      isFile <- doesFileExist $ fullPath
      if isFile && takeExtension name == ".yaml" then
        pure $ Just fullPath
      else
        pure Nothing
    aCatalog <- liftIO $
      loadYamlSettings (sort $ catMaybes yamls) [] ignoreEnv

    withPools \pools -> do
      update "Loading textures"
      -- Batch 1
      (_fontKey, fonts) <- Font.allocateCollection pools GameFont.configs
      (_textureKey, textures) <- Texture.allocateCollectionWith (Ktx1.load pools) Textures.sources

      let
        aFonts = fmap Font.container fonts
        aTextures = CombinedTextures.Collection
          { textures = textures
          , fonts    = fmap Font.texture fonts
          }

      -- Batch 2
      update "Loading cubes"
      (_cubeKey, aCubeMaps) <- Texture.allocateCollectionWith (Ktx1.load pools) CubeMap.sources

      -- Batch 3
      aModels <- loadModels pools update
      -- aMaterials <- collectMaterials update aModels

      update "Assets loaded"
      pure Assets{..}

loadModels
  :: ( MonadResource m
     , MonadVulkan env m
    --  , HasLogFunc env
     )
  => Queues Vk.CommandPool
  -> (Text -> m ())
  -> m GameModel.Collection
loadModels pools updateProgress = do
  updateProgress "Loading models"
  fmap snd $! GameModel.allocateCollection pools

-- collectMaterials
--   :: Monad m
--   => (Text -> m ())
--   -> GameModel.Collection
--   -> m MaterialCollection
-- collectMaterials updateProgress loadedModels = do
--   updateProgress "Collecting materials..."

--   pure $
--     Collect.sceneMaterials
--       loadedModels
--       (CombinedTextures.textures Combined.indices)
--       Nothing
