{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Texture
  ( Collection(..)
  , TextureCollection
  , sources
  ) where

import RIO

import Data.Distributive (Distributive(..))
import Data.Distributive.Generic (genericCollect)
import Data.Functor.Rep (Co(..), Representable)
import GHC.Generics (Generic1)
import Resource.Source (Source)
import Resource.Texture (Texture, Flat)

import Global.Resource.Texture.Base qualified as Base

data Collection a = Collection
  { base :: Base.Collection a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Co Collection)
  deriving anyclass (Representable)

instance Distributive Collection where
  collect = genericCollect

type TextureCollection = Collection (Int32, Texture Flat)

sources :: Collection Source
sources = Collection
  { base = Base.sources
  }
