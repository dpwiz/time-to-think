{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.CubeMap
  ( Collection(..)
  , CubeMapCollection
  , Ids
  , Textures
  , sources
  , indices
  , numCubes
  ) where

import RIO

import Data.Distributive (Distributive(..))
import Data.Distributive.Generic (genericCollect)
import Data.Functor.Rep (Co(..), Representable)
import GHC.Generics (Generic1)
import Global.Resource.CubeMap.Base qualified as Base

import Resource.Collection (enumerate, size)
-- import Resource.Static qualified as Static
import Resource.Texture (Texture, CubeMap)
import Resource.Source (Source)

data Collection a = Collection
  { base     :: Base.Collection a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Co Collection)
  deriving anyclass (Representable)

instance Distributive Collection where
  collect = genericCollect

type CubeMapCollection = Collection (Int32, Texture CubeMap)
type Ids               = Collection Int32
type Textures          = Collection (Texture CubeMap)

-- Static.filePatterns Static.Files "data/cubemaps"

sources :: Collection Source
sources = Collection
  { base     = Base.sources
  }

indices :: Collection Int32
indices = fmap fst $ enumerate sources

numCubes :: Num a => a
numCubes = size sources
