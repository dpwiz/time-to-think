{-# LANGUAGE OverloadedLists #-}

module Global.Resource.Model where

import RIO.Local

import Engine.Vulkan.Types (MonadVulkan, Queues)
import Geometry.Quad qualified as Quad
import Render.Unlit.Colored.Model qualified as UnlitColored
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

data Collection = Collection
  { zeroTransform :: Buffer.Allocated 'Buffer.Staged Transform

  , shroud   :: UnlitColored.Model 'Buffer.Staged
  , task     :: UnlitColored.Model 'Buffer.Staged
  , problem  :: UnlitColored.Model 'Buffer.Staged
  , solution :: UnlitColored.Model 'Buffer.Staged
  , focus    :: UnlitColored.Model 'Buffer.Staged -- TODO: animated
  }

allocateCollection
  :: ( MonadVulkan context m
     , Resource.MonadResource m
     )
  => Queues Vk.CommandPool
  -> m (Resource.ReleaseKey, Collection)
allocateCollection pools = do
  context <- ask

  zeroTransform <-
    Buffer.createStaged
      context
      pools
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      1
      [mempty]
  _zeroTransformKey <- Resource.register $
    Buffer.destroy context zeroTransform

  (shroudKey, shroud)     <- allocateQuadColored context pools $ vec4 0    0    0    0.9
  (taskKey, task)         <- allocateQuadColored context pools $ vec4 0.9  0.9  0.9  0.9
  (problemKey, problem)   <- allocateQuadColored context pools $ vec4 0.9  0.75 0.25 0.9
  (solutionKey, solution) <- allocateQuadColored context pools $ vec4 0.25 0.9  0.75 0.9
  (focusKey, focus)       <- allocateQuadColored context pools $ vec4 0.9  0.25 0.75 0.9

  key <- Resource.register $
    traverse_ @[] Resource.release
      [ shroudKey
      , taskKey
      , problemKey
      , solutionKey
      , focusKey
      ]
  pure (key, Collection{..})

allocateQuadColored
  :: ( MonadVulkan context m
     , Resource.MonadResource m
     )
  => context
  -> Queues Vk.CommandPool
  -> Vec4
  -> m (Resource.ReleaseKey, UnlitColored.Model 'Buffer.Staged)
allocateQuadColored context pools color = do
  quad <- Model.createStagedL
    context
    pools
    (Quad.toVertices $ Quad.coloredQuad color)
    Nothing

  quadKey <- Resource.register $
    Model.destroyIndexed context quad

  pure (quadKey, quad)
